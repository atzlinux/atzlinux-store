"""main"""
import os
import signal
import threading
import logging
from logging.handlers import TimedRotatingFileHandler, SysLogHandler
from queue import Queue

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib
# from .uiwin import AtzStoreWindow
from .newui import AtzstoreApp
from .aptworker import AptWorker

atzdir = os.getenv('HOME') + '/.atzlinux'
pidfile = atzdir + '/store.pid'
if not os.path.exists(atzdir):
    os.mkdir(atzdir)

fmt = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
console = logging.StreamHandler()
console.setFormatter(fmt)

logfile = '%s/atzstore.log' % atzdir
filelog = TimedRotatingFileHandler(logfile,
    when='D', backupCount=10, encoding='utf-8')
filelog.setFormatter(fmt)

logging.getLogger('').addHandler(console)
logging.getLogger('').addHandler(filelog)
logging.getLogger('').setLevel(logging.DEBUG)

logging.info("App started")
def getpid():
    try:
        if not os.path.exists(pidfile):
            logging.info('No pidfile %s', pidfile)
            return False
        with open(pidfile) as fp:
            pid = int(fp.read())
            if pid <= 0:
                logging.info('pid error %s', pid)
                return False
        if not os.path.exists('/proc/%d' % pid):
            logging.info('No a valid pid %s', pid)
            return False
        with open('/proc/%d/cmdline' % pid, 'r') as fp:
            cmdline1 = fp.read().split('\x00')
        with open('/proc/%d/cmdline' % os.getpid(), 'r') as fp:
            cmdline2 = fp.read().split('\x00')
        if len(cmdline1) < 2 or len(cmdline2) < 2:
            return False
        if cmdline1[1] != cmdline2[1]:
            logging.info('cmd line not equal')
            return False
        logging.info('Current pid=%d', pid)
        return pid
    except (OSError, ValueError):
        logging.info('except')
        return False

def store_main():
    """main"""
    cmd_queue = Queue()
    msg_queue = Queue()
    # win = AtzStoreWindow(cmd_queue)
    app = AtzstoreApp(cmd_queue)
    worker = AptWorker(cmd_queue, msg_queue)

    def sig_handler(signum, frame):
        """ 信号处理器 """
        print('sig=%s, %s' % (signum, frame))
        app.win.present()

    def update_msg():
        """ process messages """
        while True:
            msg = msg_queue.get()
            if msg[0] == 'refresh':
                GLib.idle_add(app.update_versions, msg[1])
            elif msg[0] == 'statusbar':
                GLib.idle_add(app.set_statusbar_text, msg[1])

    pid = getpid()
    if pid == False:
        with open(pidfile, 'w') as fp:
            try:
                fp.write(str(os.getpid()))
            except (OSError, ValueError):
                logging.error('Unable to write pid file')

        signal.signal(signal.SIGUSR1, sig_handler)
        # the message loop thread
        msg_thread = threading.Thread(target=update_msg)
        msg_thread.setDaemon(True)
        msg_thread.start()

        # Run apt worker in a seperated thread
        apt_thread = threading.Thread(target=worker.watch_queue)
        apt_thread.setDaemon(True)
        apt_thread.start()

        # Run ui in the main thread
        app.ui_init()
    else:
        os.kill(pid, signal.SIGUSR1)
        logging.info('already running at pid %d, bringing to front', pid)
