""" UI interface using glade """
import os
import logging
from subprocess import Popen


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class AtzstoreApp:
    """ Handlers for UI """
    columns = [
        ('pkg', '包名'),
        ('name', '应用名'),
        ('status', '安装状态'),
        ('dlsize', '下载大小'),
        ('instsize', '安装大小'),
        ('summary', '备注'),
    ]
    builder = Gtk.Builder()
    win = Gtk.Window()
    treeview = Gtk.TreeView()
    model = Gtk.ListStore()

    def __init__(self, cmd_queue):
        """ 初始化 """
        self.cmd_queue = cmd_queue

    def on_btn_refresh_clicked(self, widget):
        """ 更新软件包 """
        # print('on_btn_refresh_clicked clicked', widget, pkg)
        # self.load_data_as_model()
        logging.info("用户点击刷新")
        self.send_cmd('update')

    def on_btn_install_clicked(self, widget):
        """ 安装软件包 """
        # print('on_btn_install_clicked clicked', widget)
        pkg = self.get_value('pkg')
        if pkg is None:
            return
        logging.info("用户安装软件包：%s", pkg)
        self.send_cmd('install', pkg)

    def on_btn_delete_clicked(self, widget):
        """ 卸载软件包 """
        # print('on_btn_delete_clicked clicked', widget)
        pkg = self.get_value('pkg')
        if pkg is None:
            return
        logging.info("用户卸载软件包：%s", pkg)
        self.send_cmd('delete', pkg)

    def on_btn_view_more_clicked(self, widget):
        """ 查看详细日志 """
        # print('on_btn_view_more_clicked clicked', widget)
        logging.debug("点击查看详细日志")
        popover = self.builder.get_object('popover')
        popover.set_relative_to(widget)
        popover.show_all()
        popover.popup()

    def on_treeview_main_cursor_changed(self, widget):
        """ 选中表格行 """
        # print('on_treeview_main_cursor_changed', widget)
        pkg = self.get_value('pkg')
        if pkg is None:
            return
        summary = self.get_value('summary').replace('target="_blank"', '')
        l = self.builder.get_object('label_pkg_summary')
        l.set_label(summary)
        l.set_use_markup(True)

    def on_treeview_main_row_activated(self, widget, row, col):
        """ 双击表格行时 """
        # print('on_treeview_main_row_activated', widget, row, col)
        logging.info("用户双击第 %d 行。", row)
        pkg = self.get_value('pkg')
        if pkg is None:
            return
        status = self.get_value('status')
        if status == '未安装':
            self.set_value('status', '正在安装……')
            self.send_cmd('install', pkg)
        else:
            self.set_value('status', '正在卸载……')
            self.send_cmd('delete', pkg)

    def gtk_main_quit(self, widget):
        """ 用户点击关闭窗口 """
        # print('gtk_main_quit', widget)
        logging.info("软件退出，正在清理残留进程……")
        # Popen('killall apt 2>/dev/null', shell=True)
        # Popen('dpkg --configure -a 2>/dev/null', shell=True)
        self.send_cmd('quit')
        Gtk.main_quit()

    def update_versions(self, model):
        """ 收到版本数据，并刷新到表格中 """
        logging.info("界面收到版本号数据 %d 条。", len(model))
        # logging.debug("界面收到版本号数据详情：%s", model)
        self.treeview.set_model(model)

    def get_value(self, key):
        """ 从 model 获取数据 """
        (row, col) = self.treeview.get_cursor()
        if row is None or col is None:
            return
        col_id = 0
        for i, col in enumerate(self.columns):
            if key == col[0]:
                col_id = i
        return self.treeview.get_model()[row][col_id]

    def set_value(self, key, value):
        """ 更新 model 数据 """
        col_id = 0
        pkg = self.get_value('pkg')
        model = self.treeview.get_model()
        for i, col in enumerate(self.columns):
            if key == col[0]:
                col_id = i
        for i, row in enumerate(model):
            if row[0] == pkg:
                ite = model.get_iter(i)
                self.treeview.get_model().set_value(ite, col_id, value)

    def send_cmd(self, cmd, data=None):
        """ 发送命令 """
        # print('cmd', cmd, data)
        logging.debug("界面给 apt worker 发送命令：%s %s", cmd, data)
        self.cmd_queue.put_nowait((cmd, data))

    def set_statusbar_text(self, msg):
        """ 修改状态栏文字 """
        label = self.builder.get_object('label_statusbar')
        detail = self.builder.get_object('label_detail')
        label.set_text(msg)
        orig = detail.get_text()
        orig = "\n".join(orig.split("\n")[-30:])
        detail.set_text(msg if orig == '' else orig + "\n" + msg)

    def ui_init(self):
        """ 初始化用户界面 """
        ui = os.path.realpath(os.path.dirname(__file__) + "/atzstore.glade")
        if not os.path.exists(ui):
            logging.error("无法打开glade文件，请检查路径：%s", ui)
            exit(1)
        logging.info("正在加载界面文件: %s", ui)
        self.builder.add_from_file(ui)
        win = self.builder.get_object('main_win')
        if win is None:
            logging.error("glade文件中无法找到窗口数据，%s", ui)
            exit(1)
        self.builder.connect_signals(self)
        self.win = win

        self.treeview = self.builder.get_object('treeview_main')
        search_entry = self.builder.get_object('search_entry')
        # self.load_data_as_model()
        # treeview.set_model(self.model)
        self.treeview.set_search_entry(search_entry)
        def search_func(model, column, key, row_iter):
            """search"""
            row = list(model[row_iter])
            k = key.lower()
            for i in [0, 1, 5]:
                if k in row[i].lower():
                    return False
            return True
        self.treeview.set_search_equal_func(search_func)
        for i, col in enumerate(self.columns):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(col[1], renderer, text=i)
            column.set_max_width(600)
            column.set_resizable(True)
            column.set_sort_column_id(i)
            self.treeview.append_column(column)
        self.send_cmd('update')

        win.maximize()
        win.show_all()
        Gtk.main()
