"""
apt utils
"""
import os
import time
import logging
import platform
import json, re
from urllib.request import urlopen


from subprocess import Popen, PIPE, STDOUT
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import apt

from .newui import AtzstoreApp


class AptWorker():
    """apt worker"""
    def __init__(self, cmd_queue, msg_queue):
        logging.debug('Init apt worker finish')
        self.data = []
        self.process = 0
        self.cmd_queue = cmd_queue
        self.msg_queue = msg_queue

    def pexec(self, cmd, cb_stdout):
        """Execute the cmd and callback each line of stdout and stderr"""
        os.putenv('DEBIAN_FRONTEND','noninteractive')
        os.putenv('DEBCONF_NONINTERACTIVE_SEEN','true')
        self.process = Popen(cmd, stdout=PIPE, stderr=STDOUT, shell=True)
        for line in iter(self.process.stdout.readline, b''):
            line = str(line, encoding='UTF-8')
            if len(line.strip()) == 0 or 'stable CLI interface' in line:
                continue
            date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            logmsg = '[%s] %s' % (date, line)
            logging.info(logmsg)

            cb_stdout('%s' % line.strip())
        self.process.stdout.close()
        self.process.wait()

    def abort(self):
        """ 关闭窗口清理进程 """
        if self.process and self.process.poll() != 0:
            self.process.terminate()

    def push_msg(self, msg):
        """send message back to ui"""
        self.msg_queue.put(('statusbar', msg))

    def load_packages(self):
        """Loading packages from remote"""
        model = Gtk.ListStore(str, str, str, str, str, str)
        arch = platform.machine()
        url = f'https://www.atzlinux.com/allpackages-{arch}.json'
        self.push_msg('正在下载%s' % url)

        time_start = time.time_ns()
        logging.info("Loading packages info from %s", url)
        request = urlopen(url)
        content = request.read().decode('utf-8')
        time_end = time.time_ns()
        logging.info("Loaded %d bytes in %f ms from %s",
            len(content), (time_end - time_start) / 1e6, url)
        # allp = os.path.dirname(__file__) + "/allpackages.json"
        # fp = open(allp, 'r')
        # content = fp.read()
        list_data = json.loads(content)

        # pattern = re.compile('<[^>]+>', re.S)
        for data in list_data:
            data['status'] = '未安装'
            # data['summary'] = pattern.sub('', data['summary'])
            row = []
            for col in AtzstoreApp.columns:
                row.append(data[col[0]])
            model.append(row)
        return model

    def load_cache_versions(self):
        model = self.load_packages()
        cache = apt.Cache()
        cache.open(None)
        self.push_msg('正在缓存中读取版本号信息……')
        for i, row in enumerate(model):
            pkgname = row[0]
            if pkgname in cache and cache[pkgname].installed is not None:
                model.set_value(model.get_iter(i), 2, cache[pkgname].installed.version)
        self.push_msg('加载完成。')
        self.msg_queue.put_nowait(('top', '双击软件可以安装或卸载。'))
        return model

    def watch_queue(self):
        """loop for watching queue"""
        while True:
            msg = self.cmd_queue.get()
            cmd = msg[0]
            data = msg[1]
            if data is None and self.data is not None:
                data = self.data
            # logging.info('Receive cmd %s, with data %s', cmd, data)
            if cmd == 'refresh':
                self.msg_queue.put_nowait(('refresh', self.load_cache_versions()))
            if cmd == 'update':
                logging.info('正在执行更新')
                self.pexec('pkexec apt update', self.push_msg)
                self.cmd_queue.put(('refresh', ''))
            elif cmd == 'install':
                logging.info('正在安装%s', data)
                self.pexec('pkexec apt install -y %s -t buster-backports' % data, self.push_msg)
                self.cmd_queue.put(('refresh', ''))
                self.push_msg('安装完成。')
            elif cmd == 'delete':
                logging.info('正在卸载%s', data)
                self.pexec('pkexec apt remove -y %s' % data, self.push_msg)
                self.cmd_queue.put(('refresh', ''))
                self.push_msg('卸载完成。')
            elif cmd == 'quit':
                logging.info('强制关闭apt进程')
                self.abort()
