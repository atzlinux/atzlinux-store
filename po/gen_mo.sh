#!/bin/bash
for f in po/*.po; do
    p=${f/po\//usr\/share\/locale\/};
    p=${p/.po/\/LC_MESSAGES\/atzlinux-store.mo};
    echo mkdir -pv $(dirname $p)
    mkdir -pv $(dirname $p)
    echo msgfmt -o ${p} ${f};
    msgfmt -o ${p} ${f};
done